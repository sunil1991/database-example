package com.example.databaseexample.model;

public class User {

	private Integer id;
	private String name;
	private String org;
	
	public User() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOrg() {
		return org;
	}

	public void setOrg(String org) {
		this.org = org;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", org=" + org + "]";
	}
	
	
	

}

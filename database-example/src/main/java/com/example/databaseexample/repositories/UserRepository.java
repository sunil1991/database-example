package com.example.databaseexample.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.databaseexample.model.User;

@Repository
public class UserRepository {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public Integer saveUser(User user) {
		System.out.println(user);
		String sql = "INSERT INTO USER(id, name, org) VALUES( "+user.getId()+", '"+user.getName()+"', '"+ user.getOrg()+"'" +")";
		return jdbcTemplate.update(sql);

	}

	public User getUserById(Integer id){
		String sql = "SELECT * FROM USER WHERE ID= "+ id;
		User user;
		
		user = jdbcTemplate.queryForObject(sql, (rs, n)->{
			User userOne = new User();
			userOne.setId(rs.getInt(1));
			userOne.setName(rs.getString(2));
			userOne.setOrg(rs.getString(3));
			return userOne;
		});

		return user;
	}

}

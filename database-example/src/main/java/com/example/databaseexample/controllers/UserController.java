package com.example.databaseexample.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.databaseexample.model.User;
import com.example.databaseexample.repositories.UserRepository;

@RestController
public class UserController {
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping(value="/saveUser", method=RequestMethod.POST)
	public Integer saveUser(@RequestBody User user) {
		return userRepository.saveUser(user);
	}
	
	@RequestMapping(value="/getUserById", method=RequestMethod.GET)
	public User getUser(@RequestParam("id") Integer id) {
		return userRepository.getUserById(id);
	}
}
